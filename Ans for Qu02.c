#include <stdio.h>

int main()
{
    float radius,height,volume;
    float PI = 3.14;

    printf("Input height :\n");
    scanf("%f",&height);
    printf("Input radius :\n");
    scanf("%f",&radius);
    
    volume = PI*(radius*radius)*(height/3);

    printf("Volume of the cone : %lf",volume);
    return 0;

}