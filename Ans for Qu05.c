#include <stdio.h>
    
int main()
{
    int A = 10,B = 15;

    printf("Using And Operators :%d\n",A&B);
    printf("Using XOR Operator : %d\n",A^B);
    printf("Using Compliment : %d\n",~A);
    printf("Using Left Shift :%d\n",A<<3);
    printf ("Using Right Shift :%d\n",B>>3);
    return 0;

}


